
class Protein:
    def __init__(self, id, name, os, ox, pe, sv):
        self.id = id
        self.name = name
        self.os = os
        self.ox = ox
        self.pe = pe
        self.sv = sv
        self.size = 0
        self.aminoacids = ""
        self.aminoacidcounts = {
                    "Y" : 0,
                    "W" : 0,
                    "G" : 0,
                    "H" : 0,
                    "A" : 0,
                    "E" : 0,
                    "C" : 0,
                    "F" : 0,
                    "I" : 0,
                    "K" : 0,
                    "L" : 0,
                    "M" : 0,
                    "N" : 0,
                    "P" : 0,
                    "Q" : 0,
                    "R" : 0,
                    "S" : 0,
                    "T" : 0,
                    "U" : 0,
                    "V" : 0
  		                        }

    def __eq__(self, other):
        return self.size ==  other.size

    def __ne__(self, other):
        return self.size !=  other.size

    def __lt__(self, other):
         return self.size <  other.size

    def __le__(self, other):
         return self.size <= other.size

    def __gt__(self, other):
       return self.size > other.size

    def __ge__(self, other):
       return self.size >= other.size

    def __repr__(self):
       return self.name+" id: "+str(self.id)

def load_fasta(path):
    proteins = [] 
    fasta_file = open(path, "r")
    fasta_row = fasta_file.readline().rstrip()
    while fasta_row:
        if (fasta_row.startswith(">")):
            id = fasta_row[fasta_row.find("|")+1:fasta_row.rfind("|")]
            name = fasta_row[fasta_row.rfind("|")+1:fasta_row.find("OS=")-1]
            os = fasta_row[fasta_row.find("OS=")+3:fasta_row.find("OX=")-1]
            gn = fasta_row[fasta_row.find("GN=")+3:fasta_row.find("PE=")-1]
            ox = int(fasta_row[fasta_row.find("OX=")+3:fasta_row.find("GN=")-1])
            pe = int(fasta_row[fasta_row.find("PE=")+3:fasta_row.find("SV=")-1])
            sv = int(fasta_row[fasta_row.find("SV=")+3])
            proteins.append(Protein(id, name, os, ox, pe, sv)) 
        else: 
            proteins[-1].aminoacids += (fasta_row)
            proteins[-1].size += len(fasta_row)
            for aminoacid in fasta_row:
                if(aminoacid in proteins[-1].aminoacidcounts):
                    proteins[-1].aminoacidcounts[aminoacid] += 1
                else:
                    proteins[-1].aminoacidcounts[aminoacid] = 1
        fasta_row = fasta_file.readline().rstrip()
    fasta_file.close()
    return proteins

def sort_proteins_pe(proteins):
    return  sorted(proteins, key=lambda p:p.pe, reverse=True)

def sort_proteins_aa(proteins, letter_code):
    return sorted(proteins, key=lambda p:p.aminoacidcounts[letter_code], reverse=True)

def find_protein_with_motif(proteins, motif):
    proteins_with_motif = [] 
    for protein in proteins:
        if (protein.aminoacids.find(motif) != -1):
            proteins_with_motif.append(protein)
    return proteins_with_motif